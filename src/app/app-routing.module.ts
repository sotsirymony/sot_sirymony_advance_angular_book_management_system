import { CategoryListComponent } from "./category-list/category-list.component";
import { UpdateCategoryComponent } from "./update-category/update-category.component";
import { CategoryDetailsComponent } from "./category-details/category-details.component";
import { CreateCategoryComponent } from "./create-category/create-category.component";

import { BookDetailsComponent } from "./book-details/book-details.component";
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListComponent } from './book-list/book-list.component';
import { UpdateBookComponent } from './update-book/update-book.component';
import { CreateBooksComponent } from "./create-books/create-books.component";

const routes: Routes = [
  { path: '', redirectTo: 'book', pathMatch: 'full' },
  { path: 'book', component: EmployeeListComponent },
  { path: 'create_book', component:CreateBooksComponent },
  { path: 'update_book/:id', component: UpdateBookComponent },
  { path: 'details_book/:id', component: BookDetailsComponent },

  { path: 'category', component: CategoryListComponent },
  { path: 'add', component: CreateCategoryComponent },
  { path: 'update/:id', component: UpdateCategoryComponent },
  { path: 'details/:id', component: CategoryDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
