export class Book {
  id: number;
  title: string;
  author: string;
  description: string;
  thumnail: string;
  active: boolean;
}
