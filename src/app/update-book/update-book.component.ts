import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { BookService } from '../book.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-update-employee',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {

  id: number;
  employee: Book;

  constructor(private route: ActivatedRoute,private router: Router,
    private employeeService: BookService) { }

  ngOnInit() {
    this.employee = new Book();

    this.id = this.route.snapshot.params['id'];
    
    this.employeeService.getBook(this.id)
      .subscribe(data => {
        console.log(data)
        this.employee = data;
      }, error => console.log(error));
  }

  updateEmployee() {
    this.employeeService.updateBook(this.id, this.employee)
      .subscribe(data => {
        console.log(data);
        this.employee = new Book();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updateEmployee();    
  }

  gotoList() {
    this.router.navigate(['/book']);
  }
}

