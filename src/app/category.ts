export class Category {
  id: number;
  title: string;
  active: boolean;
}
