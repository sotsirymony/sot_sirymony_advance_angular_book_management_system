//import { EmployeeDetailsComponent } from '../employee-details/employee-details.component';
import { Observable } from "rxjs";
import { CategoryService } from "../category.service";
import { Category } from "../category";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';

@Component({
  selector: "app-category-list",
  templateUrl: "./category-list.component.html",
  styleUrls: ["./category-list.component.css"]
})
export class CategoryListComponent implements OnInit {
  categorys: Observable<Category[]>;
  name="mony"
  totalRecords:string;
  itemsPerpage=1;
  p:number=1;

  constructor(private categoryService: CategoryService,
    private router: Router) {}

  ngOnInit() {
    this.reloadData();
    console.log(this.categorys);
  }
  reloadData() {
    this.categorys = this.categoryService.getCategoryList();

    this.categoryService.getCategoryList()
    .subscribe(data => {
      console.log(data)
      this.categorys = data;
      this.totalRecords=data.length;
    }, error => console.log(error));
  }

  deleteCategory(id: number) {
    this.categoryService.deleteCategory(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
  goToPage():void{
    this.router.navigate(['/add']);
  }         

  categoryDetails(id: number){
    this.router.navigate(['details', id]);
  }

  updateCategory(id: number){
    this.router.navigate(['update', id]);
  }
}

