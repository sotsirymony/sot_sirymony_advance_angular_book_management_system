import { I18nServiceService } from "./../i18n-service.service";
import { TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs";
import { BookService } from "../book.service";
import { Book } from "../book";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';


@Component({
  selector: "app-book-list",
  templateUrl: "./book-list.component.html",
  styleUrls: ["./book-list.component.css"]
})
export class EmployeeListComponent implements OnInit {
  books: Observable<Book[]>;
  name="mony"
  totalRecords:string;
  itemsPerpage=1;
  p:number=1;
  title = 'ngx-i18n';

  constructor(private bookService: BookService,
    private router: Router,private translate: TranslateService, 
    private i18nService: I18nServiceService) {
      translate.setDefaultLang('en');
      translate.use('en');
    }

  ngOnInit() {
    this.reloadData();
    console.log(this.books);
    this.i18nService.localeEvent.subscribe(locale => this.translate.use(locale));   
  }
  reloadData() {
    this.bookService.getBookList()
    .subscribe(data => {
      console.log(data)
      this.books = data;
      this.totalRecords=data.length;
    }, error => console.log(error));

  }
  goToPage():void{
    this.router.navigate(['/create_book']);
  }

  deleteBook(id: number) {
    this.bookService.deleteBook(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  bookDetails(id: number){
    this.router.navigate(['details_book', id]);
  }

  updateBook(id: number){
    this.router.navigate(['update_book', id]);
  }
  
  changeLocale(locale: string) {
    this.i18nService.changeLocale(locale);   
  }
}
