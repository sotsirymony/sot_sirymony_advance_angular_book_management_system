import { I18nServiceService } from "./i18n-service.service";
import { Component } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Book Management System';
  constructor(
    translate: TranslateService,
    private i18nService: I18nServiceService
  ) {
    translate.setDefaultLang('ar');
    translate.use('ar');
  }
  changeLocale(locale: string) {
    this.i18nService.changeLocale(locale);   
  }
}
