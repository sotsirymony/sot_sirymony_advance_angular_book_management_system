import { Component, OnInit } from '@angular/core';
import { Category as Category } from '../category';
import { CategoryService as CategoryService } from '../category.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.css']
})
export class UpdateCategoryComponent implements OnInit {

  id: number;
  category: Category;

  constructor(private route: ActivatedRoute,private router: Router,
    private categoryService: CategoryService) { }

  ngOnInit() {
    this.category = new Category();

    this.id = this.route.snapshot.params['id'];
    
    this.categoryService.getCategory(this.id)
      .subscribe(data => {
        console.log(data)
        this.category = data;
      }, error => console.log(error));
  }

  updateCategory() {
    this.categoryService.updateCategory(this.id, this.category)
      .subscribe(data => {
        console.log(data);
        this.category = new Category();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updateCategory();    
  }

  gotoList() {
    this.router.navigate(['/category']);
  }
}
